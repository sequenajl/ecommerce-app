import { useState, useEffect, useContext } from 'react';
import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useParams } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	const {id} = useParams();
	let prod = {id}.id;
	console.log({id})
	useEffect(()=>{
		fetch(`https://frozen-bayou-69638.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData =>{	
			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])
	
	const purchase = () => {
		return (
			Swal.fire({
				icon: "success",
				title: 'Added to Cart!'
			})

		);
	};
	const redirect = () => {
		return (
			window.location.href = "/login"
			)
	}
	return(
		<div className="min-vh-100 pView">
			<Row>
				<Col>
					<Container className="mt-5">
						<Card className="text-center">
							<Card.Body>
								<Card.Title>
									<h2> {productInfo.name}</h2>
								 <Card.Img variant="top" className="viewProd" src={`/${prod}.jpg`}/>
								</Card.Title>
								<Card.Subtitle>
									<h6 className="my-4"> Description: </h6>
								</Card.Subtitle>
								<Card.Text>
									{productInfo.description}
								</Card.Text>
								<Card.Subtitle>
									<h6 className="my-4"> Price: ₱{productInfo.price} </h6>
								</Card.Subtitle>
							</Card.Body>


							<Button variant="secondary" className="btn-block purch" 
							onClick={user.id ? purchase : redirect} >
							 Add to Cart
							</Button>
						</Card>		
					</Container>		
				</Col>			
			</Row>
		</div>
		);
};