import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import {Form, Row, Col, Button, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail ] = useState('');
	const [password, setPassword ] = useState('');

	let addressSign = email.search('@');
	let dns = email.search('.com')
	
	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);

	useEffect(()=>{
		
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password !== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
			setIsActive(false)
		}
	},[email, password, addressSign, dns])

	const loginUser = async (event)=>{
		event.preventDefault()

			fetch('https://frozen-bayou-69638.herokuapp.com/users/login', {
				method: 'POST',
				headers: {'Content-Type':'application/json'},
				body: JSON.stringify({
				    email: email,
				    password: password
				})
			}).then(res => res.json()).then(dataNaJson => {
				
				let token = dataNaJson.access;
				if (typeof token !== 'undefined') {
					
					localStorage.setItem('accessToken',token);
					 fetch('https://frozen-bayou-69638.herokuapp.com/users/details', {
					    headers: {
					      Authorization: `Bearer ${token}`
					    }
					  }).then(res => res.json()).then(convertedData => {
					    if (typeof convertedData._id !== "undefined") {
					       setUser({
					        id: convertedData._id,
					        isAdmin: convertedData.isAdmin
					      });

						window.location.href = "/";
					    } else {
					      setUser({
					        id: null,
					        isAdmin: null
					      });
					    }
					  });

				} else {
					Swal.fire({
						icon: 'error',
						title: 'Check your Credentials',
						text: 'Contact Admin if problem persist!'
					})
				}
			})
		};
	
	return(
		user.id
		?	<Navigate to="/" replace={true} />
		:

		<div className="loginBg">
			
			<Container className="min-vh-100">
			<Row>
					<Col className="mt-4">
						<div className="loginForm">
				<h1 className="text-center mt-5">LOG IN</h1>
				<Form className="login" onSubmit={e=> loginUser(e)} >
					{/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" 
						placeholder="Enter Email Here" 
						required 
						value={email} 
						onChange={event=>{setEmail(event.target.value)} } />
							{
							isValid ?
							<h6 className="text-success"> Email is Valid </h6>
							:
							<h6 className="text-mute"> Email is Invalid </h6>
						}
					</Form.Group>

					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" 
						placeholder="Enter Password Here" 
						required
						value={password} 
						onChange={e=>{setPassword(e.target.value)}} />
					</Form.Group>

					<div className="d-flex justify-content-center">
					{
						isActive ?
						<Button type="submit" className="my-3 mb-5 loginButton" variant="secondary">Login</Button>
						:
						<Button type="submit" className="my-3 mb-5 loginButton" variant="secondary" disabled>Login</Button>
					}
					</div>
					<div className="d-flex justify-content-center">
						No Account?{' '}<Link to="/register" className="mx-3 create"> Create Your Account</Link>
					</div>
				</Form>
				</div>
				</Col>
			</Row>	
			</Container>
		</div>
		);
};